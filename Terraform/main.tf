# Generate a random name for the resource group
resource "random_pet" "rg_name" {
  prefix = var.resource_group_name_prefix
}

# Create an Azure resource group
resource "azurerm_resource_group" "rg" {
  location = var.resource_group_location   # Specify the location for the resource group
  name     = random_pet.rg_name.id         # Use the randomly generated name for the resource group
}

# Generate a random name for the Azure Kubernetes Cluster
resource "random_pet" "azurerm_kubernetes_cluster_name" {
  prefix = "cluster"
}

# Generate a random DNS prefix for the Azure Kubernetes Cluster
resource "random_pet" "azurerm_kubernetes_cluster_dns_prefix" {
  prefix = "dns"
}

# Create an Azure Kubernetes Cluster
resource "azurerm_kubernetes_cluster" "k8s" {
  location            = azurerm_resource_group.rg.location         # Specify the location for the Kubernetes cluster
  name                = random_pet.azurerm_kubernetes_cluster_name.id  # Use the randomly generated name for the Kubernetes cluster
  resource_group_name = azurerm_resource_group.rg.name             # Specify the resource group for the Kubernetes cluster
  dns_prefix          = random_pet.azurerm_kubernetes_cluster_dns_prefix.id  # Use the randomly generated DNS prefix for the Kubernetes cluster

  # Specify the identity configuration for the Kubernetes cluster
  identity {
    type = "SystemAssigned"
  }

  # Specify the configuration for the default node pool
  default_node_pool {
    name       = "agentpool"               # Specify the name of the default node pool
    vm_size    = "Standard_D2_v2"          # Specify the VM size for the default node pool
    node_count = var.node_count            # Specify the number of nodes for the default node pool
  }

  # Specify the Linux profile configuration for the Kubernetes cluster
  linux_profile {
    admin_username = var.username    # Specify the admin username for the Linux profile

    # Configure SSH key for the Linux profile
    ssh_key {
      # Specify the SSH public key data
      key_data = "ssh-rsa AAAA... <user>@admin-linux.local"  
    }
  }

  # Specify the network profile configuration for the Kubernetes cluster
  network_profile {
    network_plugin    = "kubenet"   # Specify the network plugin for the Kubernetes cluster
    load_balancer_sku = "standard"   # Specify the load balancer SKU for the Kubernetes cluster
  }
}
terraform {
  # Specifies the required Terraform version to be used
  required_version = ">=1.0"
  # Specifies the required providers and their versions
  required_providers {
    # Azure provider
    azapi = {
      source  = "azure/azapi"
      # Specifies the version constraint for the provider
      version = "~>1.5"
    }
    # AzureRM provider
    azurerm = {
      source  = "hashicorp/azurerm"
      version = "~>3.0"
    }
    random = {
      source  = "hashicorp/random"
      version = "~>3.0"
    }
    time = {
      source  = "hashicorp/time"
      version = "0.9.1"
    }
  }
}
# Configuration block for the azurerm provider
provider "azurerm" {
  features {}
}
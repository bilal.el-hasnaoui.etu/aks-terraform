# Déploiement d'un cluster AKS avec Terraform et application de microservices avec Kubernetes

Ce projet vise à déployer un cluster Azure Kubernetes Service (AKS) à l'aide de Terraform et à déployer une application de microservices simple sur ce cluster en utilisant Kubernetes.

## Prérequis

- Un compte Azure avec les autorisations nécessaires pour créer des ressources (Creer un compte azure student c'est free)
### Terraform installé sur votre machine locale. 
[Installation de Terraform sur Linux](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)

### Connexion à Azure CLI.
[Installation de az CLI sur Linux](https://learn.microsoft.com/fr-fr/cli/azure/install-azure-cli-linux?pivots=apt)
    ```bash
    az login
    ```
- La command ouvre une tab sur le navigateur :
![az login commande](imgs/az-login.png)

- Continue les processus de login sur le naviugateur :
<div style="text-align:center;">
  <img src="imgs/az-login-browser.png" alt="az login navigateur" width="400">
</div>


## Configuration

1. **Clonez ce référentiel sur votre machine locale :**
    ```bash
    git clone https://gitlab.univ-lille.fr/bilal.el-hasnaoui.etu/aks-terraform
    cd aks-terraform
    ```
2. **Configuration Terraform :**
### main.tf
- Fichier principal de configuration Terraform.
- Contient les définitions des ressources d'infrastructure à provisionner.

### outputs.tf
- Définit les sorties à afficher après l'application de la configuration Terraform.
- Utile pour fournir des informations telles que les adresses IP, les noms de domaine, etc.

### providers.tf
- Spécifie les fournisseurs de services cloud utilisés pour gérer l'infrastructure.
- Contient les détails d'authentification et de configuration spécifiques au fournisseur.

### ssh.tf
- Peut contenir des configurations liées à l'accès SSH, comme les clés SSH ou les paramètres SSH pour les instances.
- Utilisé pour configurer l'accès SSH aux instances et éventuellement les scripts de provisionnement via SSH.

### variables.tf
- Déclare les variables d'entrée permettant de paramétrer la configuration Terraform.
- Permet de rendre la configuration plus flexible et réutilisable en définissant les variables avec leurs types et valeurs par défaut.

3. **Plan Terraform :**
    ```bash
    terraform plan -out main.tfplan
    ```

## Déploiement du Cluster AKS

1. **Utilisez Terraform pour déployer le cluster AKS :**
    ```bash
    terraform apply main.tfplan
    ```

2. **Après le déploiement, Terraform affichera les détails du cluster AKS, y compris le nom, l'emplacement et d'autres informations utiles.**

## Déploiement de l'application de microservices

1. **Accédez au répertoire contenant les configurations Kubernetes :**
    ```bash
    cd deployments
    ```

2. **Déployez l'application de microservices en utilisant `kubectl` :**
    ```bash
    kubectl apply -f order-service.yaml product-service.yaml rabbitmq.yaml store-front.yaml
    cd services
    kubectl apply -f order-service.yaml product-service.yaml rabbitmq.yaml store-front.yaml
    ```

3. **Vérifiez que les déploiements et les services sont en cours d'exécution :**
    ```bash
    kubectl get deployments
    kubectl get services
    ```

## Accès à l'application

1. **Une fois l'application déployée avec succès, obtenez l'adresse IP externe :**
    ```bash
    kubectl get svc store-front
    ```

2. **Accédez à l'application via l'adresse IP :**

## Nettoyage

Pour éviter des frais inattendus, assurez-vous de supprimer toutes les ressources une fois que vous avez terminé :

```bash
terraform destroy